# Kubernetes resource-limited Namespace

Kubernetes Namespace with Resource Quotas to avoid accidents from hogging all resources and continuously scaling the cluster up.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_limit_range.default-container](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/limit_range) | resource |
| [kubernetes_limit_range.default-pod](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/limit_range) | resource |
| [kubernetes_namespace.namespace](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_resource_quota.quota](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/resource_quota) | resource |
| [kubernetes_role.role](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role.role-monitoring](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role_binding.binding](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |
| [kubernetes_role_binding.binding-monitoring](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |
| [kubernetes_service_account.account](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |
| [kubernetes_secret.secret](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/secret) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_max_cpu"></a> [max\_cpu](#input\_max\_cpu) | Maximum amount of CPU resource that can be used by the Pods in the Namespace. | `number` | n/a | yes |
| <a name="input_max_memory"></a> [max\_memory](#input\_max\_memory) | Maximum amount of memory resource that can be used by the Pods in the Namespace. | `string` | n/a | yes |
| <a name="input_monitoring_namespace"></a> [monitoring\_namespace](#input\_monitoring\_namespace) | n/a | `string` | `null` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Name for the Kubernetes Namespace resource to create. | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | Name of the project, used as a label for various Kubernetes resources. | `string` | n/a | yes |
| <a name="input_project_slug"></a> [project\_slug](#input\_project\_slug) | Slug used for generating various Kubernetes resource names. | `string` | n/a | yes |
| <a name="input_quota_container_default"></a> [quota\_container\_default](#input\_quota\_container\_default) | Default resource limits for Containers in the Namespace. | <pre>object({<br>    cpu    = string<br>    memory = string<br>  })</pre> | `null` | no |
| <a name="input_quota_container_default_request"></a> [quota\_container\_default\_request](#input\_quota\_container\_default\_request) | Default resource requests for Containers in the Namespace. | <pre>object({<br>    cpu    = string<br>    memory = string<br>  })</pre> | `null` | no |
| <a name="input_quota_pod_default"></a> [quota\_pod\_default](#input\_quota\_pod\_default) | Default resource requests for Pods in the Namespace. | <pre>object({<br>    cpu    = string<br>    memory = string<br>  })</pre> | `null` | no |
| <a name="input_quota_pod_default_request"></a> [quota\_pod\_default\_request](#input\_quota\_pod\_default\_request) | Default resource limits for Pods in the Namespace. | <pre>object({<br>    cpu    = string<br>    memory = string<br>  })</pre> | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_namespace"></a> [namespace](#output\_namespace) | Name of the Namespace. |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | Name of the Role bound to the admin Service Account. |
| <a name="output_token"></a> [token](#output\_token) | Token for authenticating the Service Account with admin rights over the Namespace. |
| <a name="output_user"></a> [user](#output\_user) | Name of the Service Account with admin rights over the Namespace. |
<!-- END_TF_DOCS -->
