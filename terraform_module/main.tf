locals {
  project_label               = replace(var.project_name, " ", "")
  pod_security_standard_label = var.namespace_pod_security_standard == null ? {} : { "pod-security.kubernetes.io/enforce" = var.namespace_pod_security_standard }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    labels = merge(
      { project = local.project_label },
      local.pod_security_standard_label
    )

    name = var.namespace
  }
}

resource "kubernetes_service_account" "account" {
  metadata {
    name      = "${var.project_slug}-admin"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  automount_service_account_token = true
}

resource "kubernetes_secret" "secret" {
  metadata {
    name      = "${kubernetes_service_account.account.metadata[0].name}-token"
    namespace = kubernetes_namespace.namespace.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = "${var.project_slug}-admin"
    }
  }
  type = "kubernetes.io/service-account-token"
}

data "kubernetes_secret" "secret" {
  metadata {
    name      = kubernetes_secret.secret.metadata[0].name
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
}

resource "kubernetes_role" "role" {
  metadata {
    name = "${var.project_slug}-role"
    labels = {
      project = local.project_label
    }
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }

  rule {
    api_groups = [""]
    resources = [
      "pods", "pods/log", "pods/portforward", "pods/exec",
      "endpoints", "services",
      "configmaps", "secrets",
      "persistentvolumeclaims"
    ]
    verbs = ["*"]
  }

  rule {
    api_groups = ["apps", "batch"]
    resources  = ["*"]
    verbs      = ["*"]
  }

  rule {
    api_groups = ["networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["*"]
  }

  dynamic "rule" {
    for_each = var.additionnal_role_rules
    content {
      api_groups = rule.value.api_groups
      resources  = rule.value.resources
      verbs      = rule.value.verbs
    }
  }
}

resource "kubernetes_role_binding" "binding" {
  metadata {
    name = "${kubernetes_role.role.metadata[0].name}-binding"
    labels = {
      project = local.project_label
    }
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.role.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.account.metadata[0].name
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
}

resource "kubernetes_resource_quota" "quota" {
  metadata {
    name      = "${var.project_slug}-quota"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }

  spec {
    hard = {
      "requests.cpu" : var.max_cpu_requests
      "requests.memory" : var.max_memory_requests
      "limits.cpu" : var.max_cpu_limits
      "limits.memory" : var.max_memory_limits
    }
  }
}

resource "kubernetes_limit_range" "default_container" {
  count = (
    var.default_container_cpu_limits == null
    && var.default_container_cpu_requests == null
    && var.default_container_memory_limits == null
    && var.default_container_memory_requests == null
  ) ? 0 : 1
  metadata {
    name      = "default-container"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  spec {
    limit {
      type = "Container"
      default = {
        cpu    = var.default_container_cpu_limits
        memory = var.default_container_memory_limits
      }
      default_request = {
        cpu    = var.default_container_cpu_requests == null ? var.default_container_cpu_limits : var.default_container_cpu_requests
        memory = var.default_container_memory_requests == null ? var.default_container_memory_limits : var.default_container_memory_requests
      }
    }
  }
}

moved {
  from = kubernetes_limit_range.default-container
  to   = kubernetes_limit_range.default_container
}
moved {
  from = kubernetes_role_binding.binding-monitoring
  to   = kubernetes_role_binding.binding_monitoring
}
moved {
  from = kubernetes_role.role-monitoring
  to   = kubernetes_role.role_monitoring
}
