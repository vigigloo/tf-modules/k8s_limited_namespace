variable "namespace" {
  type = string

  description = "Name for the Kubernetes Namespace resource to create."
}

variable "project_name" {
  type = string

  description = "Name of the project, used as a label for various Kubernetes resources."
}

variable "project_slug" {
  type = string

  description = "Slug used for generating various Kubernetes resource names."
}

variable "max_cpu_requests" {
  type    = string
  default = null

  description = "Maximum value for the total CPU requested by the containers in the namespace."
}
variable "max_memory_requests" {
  type    = string
  default = null

  description = "Maximum value for the total memory requested by the containers in the namespace."
}
variable "max_cpu_limits" {
  type    = string
  default = null

  description = "Maximum value for the total CPU limits for the containers in the namespace."
}
variable "max_memory_limits" {
  type    = string
  default = null

  description = "Maximum value for the total memory limits for the containers in the namespace."
}

variable "default_container_cpu_requests" {
  type    = string
  default = null

  description = "Default CPU requests value for containers with unspecified resources."
}
variable "default_container_memory_requests" {
  type    = string
  default = null

  description = "Default memory requests value for containers with unspecified resources."
}
variable "default_container_cpu_limits" {
  type    = string
  default = null

  description = "Default CPU limits value for containers with unspecified resources."
}
variable "default_container_memory_limits" {
  type    = string
  default = null

  description = "Default memory limits value for containers with unspecified resources."
}

variable "namespace_pod_security_standard" {
  type    = string
  default = "baseline"

  description = "Default Pod Security Standard to apply to the namespace. Values can be privileged, baseline or restricted. Value can be null to prevent setting the label on the namespace."
  validation {
    condition     = var.namespace_pod_security_standard == null || contains(["privileged", "baseline", "restricted"], var.namespace_pod_security_standard)
    error_message = "The Pod Security Standard can be null, \"privileged\", \"baseline\" or \"restricted\"."
  }
}

variable "additionnal_role_rules" {
  type = list(object({
    api_groups = list(string)
    resources  = list(string)
    verbs      = list(string)
  }))
  default = []
}
