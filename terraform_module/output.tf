output "user" {
  value = kubernetes_role_binding.binding.subject[0].name

  description = "Name of the Service Account with admin rights over the Namespace."
}

output "token" {
  value     = lookup(data.kubernetes_secret.secret.data, "token")
  sensitive = true

  description = "Token for authenticating the Service Account with admin rights over the Namespace."
}

output "namespace" {
  value = kubernetes_namespace.namespace.metadata[0].name

  description = "Name of the Namespace."
}

output "role_name" {
  value = "${var.project_slug}-role"

  description = "Name of the Role bound to the admin Service Account."
}
